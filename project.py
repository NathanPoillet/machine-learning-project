# -*- coding: utf-8 -*-
"""
Created on Sat Nov 26 10:32:35 2022

@author: CYTech Student
"""


#import of librairies 
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy.stats import norm
from scipy.stats import ttest_ind
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVC


#import the dataset
dataset = pd.read_csv("dataset.csv",sep=",")


#shuffle the dataset
#dataset = dataset.sample(frac = 1)




#putting a testing set aside
split_point = int(0.70*len(dataset))

test_data= dataset[split_point:]

#split the data into train and valid set
dataset = dataset[:split_point]
split_point2 = int(0.80*len(dataset))

train_data = dataset[:split_point2]
val_data = dataset[split_point2:]

#copying the train validation and test data, as we will change this variable.
train_data_with_labels = train_data.copy()
val_data_with_labels = val_data.copy()
test_data_with_labels=test_data.copy()

def print_qq_plot(train_data,val_data,columns):
    plt.style.use("dark_background")
    figure = plt.figure(constrained_layout=True,figsize=(30, 15))
    figure.suptitle('\nQQ plots\n',fontsize=50)
    
    # create 1xlen(columns) subfigs
    subfigs = figure.subfigures(nrows=1, ncols=len(columns))
    
    for row, subfig in enumerate(subfigs):
    
        # create 2x1 subplots per subfig
        axs = subfig.subplots(nrows=2, ncols=1,)
        for col, ax in enumerate(axs):
            if(col==0):
                
                column,isnum = columns[row]
                if(isnum):
                    train_data.hist(column=column,bins=20,ax=ax)
                    ax.set_title( column + " training",fontsize=30)
                else:
                    data = train_data[column]
                    data.groupby(data).count().plot(ax=ax, kind='bar',xlabel="")
                    ax.set_title(column + " training",fontsize=30)

    
            else: 
                column,isnum = columns[row]
                if(isnum):
                    val_data.hist(column=column,bins=20,ax=ax)
                    ax.set_title('\n' + column + " validation",fontsize=30)
                else:
                    data = val_data[column]
                    data.groupby(data).count().plot(ax=ax, kind='bar',xlabel="")
                    ax.set_title('\n' + column + " validation",fontsize=30)



print_qq_plot(train_data,val_data,[("valence",True),("energy",True),("track_genre",False)])



# DESCRIPTIVE ANALYSIS




labelencoder=LabelEncoder()

#changing the track genre with numbers
train_data["track_genre"] = labelencoder.fit_transform(train_data["track_genre"])
val_data["track_genre"] = labelencoder.fit_transform(val_data["track_genre"])
test_data["track_genre"] = labelencoder.fit_transform(test_data["track_genre"])




copy_data = train_data
copy_val = val_data
copy_test = test_data

#deleting columns that are strings
cols=[]
for column in train_data:
    if train_data[column].dtype=="object":
        cols.append(column)
        
copy_data = train_data.drop(columns=cols)
copy_val = val_data.drop(columns=cols)
copy_test = test_data.drop(columns=cols)


#deleting columns that are not numbers
del copy_data["Unnamed: 0"]
del copy_data["explicit"]

del copy_val["Unnamed: 0"]
del copy_val["explicit"]

del copy_test["Unnamed: 0"]
del copy_test["explicit"]

#normalising the dataset
data = (copy_data - copy_data.min())/(copy_data.max()-copy_data.min())
data_validation = (copy_val - copy_val.min())/(copy_val.max()-copy_val.min())
data_test = (copy_test - copy_test.min())/(copy_test.max()-copy_test.min())

# printing histograms
train_data.hist(column="valence",bins=20)
train_data.hist(column="instrumentalness",bins=20)
train_data.hist(column="duration_ms",bins=20)
train_data.hist(column="loudness",bins=20)
train_data.hist(column="tempo",bins=20)

#printing the heatmap
plt.figure(figsize=(14,12))
sns.heatmap(data.corr(),linewidths=.1,cmap="YlGnBu", annot=True)
plt.yticks(rotation=0);
plt.show()


#computing range, mean, quantile and std
range_column = copy_data.max() - train_data.min()
sample_mean_data = copy_data.mean()
sample_std_data = copy_data.std()
quantile = copy_data.quantile()



# PROBABILITY DISTRIBUTION


x_axis = np.arange(0, 1, 0.01)

#plotting the distribution of loudness
plt.figure(figsize=(14,12))
sns.kdeplot(data["loudness"],color="red")

sample_mean = data["loudness"].mean()
sample_std = data["loudness"].std()
plt.plot(x_axis, norm.pdf(x_axis, sample_mean,sample_std))
plt.show()


#plotting the distribution of loudness
plt.figure(figsize=(14,12))
sns.kdeplot(data["duration_ms"],color="red")

sample_mean2 = data["duration_ms"].mean()
sample_std2 = data["duration_ms"].std()
plt.plot(x_axis, norm.pdf(x_axis, sample_mean2,sample_std2))
plt.legend()



# hypothesis testing.

# pvalue <0.05 -> reject H0, so rock and metal are not as danceable.
def Hypothesis_one(train_data_with_labels):
    print("is metal as good as rock for dancing ?")

    columns = train_data_with_labels.columns.tolist()
    for column in columns:
        if column!="track_genre" and column!="danceability":
            del train_data_with_labels[column]
    
    df_rock = train_data_with_labels[train_data_with_labels['track_genre'].str.contains('rock')]
    df_metal = train_data_with_labels[train_data_with_labels['track_genre'].str.contains('metal')]
    
    rock = df_rock.sample(n = 2000)
    metal = df_metal.sample(n = 2000)
    
    return ttest_ind(rock["danceability"], metal["danceability"])
Hypothesis_one(train_data_with_labels.copy())


# pvalue <0.05 -> reject H0, so death metal and black metal are not danceable the same way.
def Hypothesis_two(train_data_with_labels):
     print("is death metal as worse as black metal for parties ?")
    
     columns = train_data_with_labels.columns.tolist()
     for column in columns:
         if column!="track_genre" and column!="danceability":
             del train_data_with_labels[column]
     
     df_death_metal = train_data_with_labels[train_data_with_labels['track_genre'].str.contains('death')]
     df_black_metal = train_data_with_labels[train_data_with_labels['track_genre'].str.contains('black')]
     
     death_metal = df_death_metal.sample(n = 200)
     black_metal = df_black_metal.sample(n = 200)
     return ttest_ind(death_metal["danceability"], black_metal["danceability"])

Hypothesis_two(train_data_with_labels.copy())







# K neighbor with danceability
to_drop = ['popularity', 'duration_ms', 'danceability', 'key',
       'loudness', 'mode', 'speechiness', 
       'liveness', 'tempo', 'time_signature']

predict = "danceability"
X_train = data.drop(columns=to_drop)
X_validation = data_validation.drop(columns=to_drop)
Y_train = data[[predict]].values.ravel()
Y_validation = data_validation[[predict]].values.ravel()

X_test = data_test.drop(columns=to_drop)
Y_test = data_test[[predict]].values.ravel()


#We will use quadratic error.
def calculate_error(Y_pred, Y_actual):
    error = 0
    for i in range(len(Y_pred)):
        error += abs(Y_pred[i] - Y_actual[i])**2
    return error / len(Y_pred)


# This needs to be run in the terminal to plot a usable plot, I don't know why.
k_errors = [np.inf] 
print("Evaluating k...")
for k in range(1,50):
    model = KNeighborsRegressor(n_neighbors=k)
    model.fit(X_train, Y_train) 
    Y_val_pred = model.predict(X_validation)
    k_errors.append(calculate_error(Y_val_pred, Y_validation))
    
plt.scatter(x=range(len(k_errors)), 
            y=k_errors)
plt.xlabel('value of k')
plt.ylabel('error')
plt.title('Error values for different k values on a KNN classifier')
plt.grid(axis='both',alpha=0.5)


# We can keep k=7
k=7


# take the labelencoder and can then return the dictionnary of track_genre related to their index
def labeldecoder(labelencoder):
    labels_int = [i for i in range(0,22)]
    labels = [i/21 for i in labels_int] 

    decode = labelencoder.inverse_transform(labels_int)
    res = dict(zip(decode,labels,))

    return res

decoder = labeldecoder(labelencoder)


#taking all the data
full_data = data.append(data_validation)

#running the K nearest-neighbors model.
model = KNeighborsRegressor(n_neighbors=k)
model.fit(X_train, Y_train) 
Y_pred = model.predict(X_validation)
print(f"Our testing error is {calculate_error(Y_pred, Y_validation)}\n\n")


#running the SVM RBF kernel model.
model2 = SVC(kernel='rbf')
model.fit(X_train, Y_train) 
Y_pred = model.predict(X_validation)
print(f"Our testing error is {calculate_error(Y_pred, Y_validation)}\n\n")


# we find the same error for both model so, let's use the KneighborsRegressor

Y_pred = model.predict(X_test)
print(f"Our testing error is {calculate_error(Y_pred, Y_test)}\n\n")




def get_key(val,dictionnary):
    for key, value in dictionnary.items():
        if val == value:
            return key
 
    return None

# Evaluating the best and worst genre for dancing. 

best_of_genre = []
worst_of_genre = []

i=0
for index, row in X_test.iterrows():
    if(Y_pred[i]>0.75):
        track_genre_number = row["track_genre"]
        track_genre = get_key(track_genre_number,decoder)
        best_of_genre.append(track_genre)

    if Y_pred[i]<0.2: 
        track_genre_number = row["track_genre"]
        track_genre = get_key(track_genre_number,decoder)
        worst_of_genre.append(track_genre)
    i=i+1
    
best_of_genre=set(best_of_genre)
worst_of_genre=set(worst_of_genre)

